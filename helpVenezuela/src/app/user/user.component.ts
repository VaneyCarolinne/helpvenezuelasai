import { UserService } from './../services/user.service';

import { Component, OnInit, Input } from '@angular/core';



@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {


  msgSuccess = false;
  msgError = false;

  constructor(private userServ: UserService) {
    this.msgSuccess = false;
    this.msgError = false;
  }

  ngOnInit(): void {
  }

  // tslint:disable-next-line: ban-types
  signIn(name: String, password: String): void{

    const newUser = {
      name_user : name,
      password_user : password
    };


    console.log('Entre!!');

    this.userServ.createUser(newUser).then(data=> {

        this.msgSuccess = data ? true : false;
        this.msgError = data ? false : true;
    }).catch(e=> {
        console.log('ERROR: ', e);
    });

  }



}
