import { UserComponent } from './../user/user.component';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  // [x: string]: any;

  // private url = 'http://localhost:8080/mdUser/createMdUser';// for Java Sring Boot backend
  private url = 'http://localhost:8000/api/mdUsers/';
  private header: HttpHeaders = new HttpHeaders
  ({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin' : '*'
  });

  constructor(private http: HttpClient) { }

  public createUser(paramUser: {}){

      return this.http.post(
        this.url,
        paramUser,
        {
          headers: this.header
        }
      ).toPromise() ;


  }

}

