import { Routes } from '@angular/router';

import { UserComponent } from '../user/user.component';
import { HomeComponent } from '../home/home.component';

export const routes: Routes = [
  { path: 'user', component: UserComponent },
  { path: 'home', component: HomeComponent },
  { path: '', redirectTo: '/user', pathMatch: 'full' },
  { path: '**', component: HomeComponent }
];
