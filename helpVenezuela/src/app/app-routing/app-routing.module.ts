import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { routes } from './routes';


// import { Routes } from '@angular/router';

// import { UserComponent } from '../user/user.component';
// import { HomeComponent } from '../home/home.component';

// export const routes: Routes = [
//   { path: 'user', component: UserComponent },
//   { path: 'home', component: HomeComponent },
//   { path: '', redirectTo: '/user', pathMatch: 'full' },
//   { path: '**', component: HomeComponent }
// ];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule]
})
export class AppRoutingModule { }
