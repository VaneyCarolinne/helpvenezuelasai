<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('/mdUsers', 'md_user@index');
// Route::get('/mdUsers/{id}', 'md_user@show');
Route::post('/mdUsers', 'md_userController@createUser');
// Route::post('/mdUsers/{id}/name_users', 'md_user@name_users');
// Route::delete('/mdUsers/{id}', 'md_user@delete');
// Route::delete('/mdUsers/{id}/name_users', 'md_user@resetname_users');
