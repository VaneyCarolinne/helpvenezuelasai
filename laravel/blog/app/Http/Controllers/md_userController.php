<?php

namespace App\Http\Controllers;
use App\md_user;
use App\Http\Resources\md_user as md_userResource;
use App\Http\Resources\md_user_Collection;

use Illuminate\Http\Request;

class md_userController extends Controller
{
    //
    public function index()
    {
        return new md_user_Collection(Player::all());
    }

    public function show($id)
    {
        return new md_user_Collection(md_user::findOrFail($id));
    }

    public function createUser(Request $request)
    {
        $request->validate([
            'name_user' => 'required|max:30',
            'password_user' => 'required|max:15',
        ]);

        // $flight = new Flight;

        // $flight->name = $request->name;

        // $flight->save();

        $md_user = md_user::create($request->all());
        // $md_user = new md_user;
        // $md_user->name_user = $request->name_user;
        // $md_user->password_user = $request->password_user;
        // $md_user->save();

        return (new md_user_Collection($md_user))
                ->response()
                ->setStatusCode(201);
    }

    // public function create($data = '')
	// {
	// 	$create = md_user::insert('md_users',$data);
	// 	return ($create == true) ? true : false;

	// }

    // public function answer($id, Request $request)
    // {
    //     $request->merge(['correct' => (bool) json_decode($request->get('correct'))]);
    //     $request->validate([
    //         'correct' => 'required|boolean'
    //     ]);

    //     $md_user = md_user::findOrFail($id);
    //     $md_user->answers++;
    //     $md_user->points = ($request->get('correct')
    //                        ? $md_user->points + 1
    //                        : $md_user->points - 1);
    //     $md_user->save();

    //     return new md_user_Collection($md_user);
    // }

    public function delete($id)
    {
        $md_user = md_user::findOrFail($id);
        $md_user->delete();

        return response()->json(null, 204);
    }

    // public function resetAnswers($id)
    // {
    //     $md_user = md_user::findOrFail($id);
    //     $md_user->answers = 0;
    //     $md_user->points = 0;

    //     return new md_user_Collection($md_user);
    // }
}
