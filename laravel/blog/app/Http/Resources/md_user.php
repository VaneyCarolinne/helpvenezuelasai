<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class md_user extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id'         => $this->id,
            'name_user'       => $this->name_user,
            'password_user'    => $this->password_user,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
