package com.helpvenezuela.sai.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.sun.istack.internal.NotNull;
import com.sun.javafx.beans.IDProperty;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.validation.constraints.Size;

@Entity
@Table(name = "md_user")
public class mdUser implements Serializable{

  private Integer id;
  private String nameUser;
  private String passwordUser;

  @Id
  @GenericGenerator(name = "mdUserSequenceGenerator", strategy = "org.hibernate.id.enhanced.TableGenerator", parameters = {
    @Parameter(name = "table_name", value = "hibernate_sequences"),
    @Parameter(name = "segment_value", value = "SEQ_MD_USER"),
    @Parameter(name = "initial_value", value = "1"),
    @Parameter(name = "increment_size", value = "1") })
  @GeneratedValue(generator = "mdUserSequenceGenerator")
  @Column(columnDefinition = "SMALLINT UNSIGNED")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Column(name = "name_user", nullable = false, length = 30)
  @NotNull
  @Size(max = 30)
  public String getNameUser() {
    return nameUser;
  }

  public void setNameUser(String nameUser) {
    this.nameUser = nameUser;
  }

  @Column(name = "password_user", nullable = false, length = 15)
  @NotNull
  @Size(max = 15)
  public  String getPasswordUser(){
    return passwordUser;
  }

  public void setPasswordUser(String passwordUser){
    this.passwordUser = passwordUser;
  }


}
