package com.helpvenezuela.sai.repository;

import com.helpvenezuela.sai.models.mdUser;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface mdUserRepository extends JpaRepository<mdUser, Integer> {


}
