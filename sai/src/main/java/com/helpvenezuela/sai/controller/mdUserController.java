package com.helpvenezuela.sai.controller;

import com.helpvenezuela.sai.dto.mdUserDTO;
import com.helpvenezuela.sai.services.mdUserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.criteria.CriteriaBuilder;

@RestController
@RequestMapping("/mdUser")
public class mdUserController {

  private final mdUserService userService;

  public mdUserController(mdUserService userService) {
    this.userService = userService;
  }

  @PostMapping("/createMdUser")
  public boolean createMdUser(@RequestBody mdUserDTO userDTO){
      boolean response = userService.createUser(userDTO);
      return response;
  }

}
