package com.helpvenezuela.sai.services;

import com.helpvenezuela.sai.dto.mdUserDTO;
import com.helpvenezuela.sai.repository.mdUserRepository;
import com.helpvenezuela.sai.models.mdUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

@Service("mdUserService")
public class mdUserService {

  private final mdUserRepository mdUserRepository ;

  @Autowired
  public mdUserService(mdUserRepository mdUserRepository) {
    this.mdUserRepository = mdUserRepository;
  }

  private final static Logger LOGGER = Logger.getLogger("com.helpvenezuela.sai.services");

  public boolean createUser(mdUserDTO userDTO){
    mdUser user = new mdUser();
    user.setId(userDTO.getId());
    user.setNameUser(userDTO.getNameUser().trim().isEmpty()  ? null : userDTO.getNameUser());
    user.setPasswordUser(userDTO.getPasswordUser().trim().isEmpty() ? null : userDTO.getPasswordUser());
    try {
      mdUserRepository.save(user);
      return true;
    }catch (Exception e){
      LOGGER.log(Level.INFO, "VANEY ERROR: exception start:  " + e);
      return false;
    }

  }
}
