package com.helpvenezuela.sai.dto;

import com.helpvenezuela.sai.models.mdUser;

import java.util.List;

public class mdUserDTO {

  private Integer id;
  private String nameUser;
  private String passwordUser;

  public mdUserDTO(){}

  public mdUserDTO(Integer id, String nameUser, String passwordUser){
    this.id = id;
    this.nameUser = nameUser;
    this.passwordUser = passwordUser;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getNameUser() {
    return nameUser;
  }

  public void setNameUser(String nameUser) {
    this.nameUser = nameUser;
  }

  public String getPasswordUser() {
    return passwordUser;
  }

  public void setPasswordUser(String passwordUser) {
    this.passwordUser = passwordUser;
  }

  public static mdUserDTO fromModel(mdUser entity){
    mdUserDTO dto = new mdUserDTO();

    dto.setId(entity.getId());
    dto.setNameUser(entity.getNameUser());
    dto.setPasswordUser(entity.getPasswordUser());

    return dto;
  }

}
