package com.helpvenezuela.sai;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jmx.export.annotation.AnnotationMBeanExporter;
import org.springframework.jmx.support.RegistrationPolicy;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.TimeZone;

@Configuration
@EnableJpaRepositories("com.helpvenezuela.sai.repository")
public class JPAConfig {

  private static final Logger logger = (Logger) LoggerFactory.getLogger(JPAConfig.class);

  @Autowired
  private Environment env;

  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory(@Qualifier("dataSource") DataSource dataSource)
    throws NamingException {
    LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
    em.setDataSource((javax.sql.DataSource) dataSource);
    em.setPackagesToScan(new String[]{"com.helpvenezuela.sai.models"});
    em.setJpaVendorAdapter(jpaVendorAdapter());
    em.setJpaProperties(jpaProperties());
    return em;
  }

  private JpaVendorAdapter jpaVendorAdapter() {

    HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
    hibernateJpaVendorAdapter.setShowSql(Boolean.TRUE);
    hibernateJpaVendorAdapter.setDatabase(Database.MYSQL);
    return hibernateJpaVendorAdapter;

  }


  private Properties jpaProperties() {

    Properties jpaProperties = new Properties();
    //Configures the used database dialect. This allows Hibernate to create SQL
    //that is optimized for the used database.
    jpaProperties.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");

    //Evitar errores de LazyInitializationException
    jpaProperties.put("hibernate.enable_lazy_load_no_trans", true);

    //Evita error de Connection createClob() is not yet implemented
    jpaProperties.put("hibernate.jdbc.lob.non_contextual_creation", true);

    jpaProperties.put("hibernate.show_sql", false);

    return jpaProperties;

  }

  /* configuracion de coneccion a base de datos*/
  @Bean
  public DataSource dataSource() throws NamingException {
    //return (DataSource) new JndiTemplate().lookup("java:comp/env/jdbc/tomcat_provided_datasource_name");

    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    //dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
    logger.info(env.getProperty("spring.datasource.url"));
    dataSource.setDriverClassName(env.getProperty("spring.datasource.driver-class-name"));
    dataSource.setUrl(env.getProperty("spring.datasource.url"));
    dataSource.setUsername(env.getProperty("spring.datasource.username"));
    dataSource.setPassword(env.getProperty("spring.datasource.password"));

    return (DataSource) dataSource;
  }

}
